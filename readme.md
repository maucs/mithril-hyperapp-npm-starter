# Mithril + Hyperapp

Using hyperapp for its state management, mithril for everything else, and a Babel transform plugin `Object.assign`

Follow the **init.sh** instructions, and then copy the `scripts` directive found in **package.json**

To generate, run `npm run build`, outputs to directory `webpack/dist`