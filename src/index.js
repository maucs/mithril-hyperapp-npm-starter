import m from 'mithril'
import { app } from "hyperapp"

let state = {
  number: 0,
  name: 'jolly go '
}

const actions = {
  up: value => state => ({number: state.number + value}),
  append: () => state => ({name: state.name + 'bafg'})
}

const view = (s, a) => {}

let main = app(state, actions, view, null)

// onclick takes a callback, so the additional () is required
const update = (f, args) => () =>
  Object.assign(state, f())

// main.up takes an arg, main.append doesn't
// main.up is wrapped in a callback
const Main = {
  view: () => m('div#main', [
    m('p', {onclick: update(() => main.up(1))}, state.number),
    m('p', {onclick: update(main.append)}, state.name),
  ])
}

m.mount(document.body, Main)